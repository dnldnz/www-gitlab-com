---
title: "Building a transparency-first AI strategy: 7 questions to ask your DevOps provider"
author: Robin Schulman
author_gitlab: robin
author_twitter: gitlab
categories: ai-ml
image_title: /images/blogimages/duo-blog-post.png
description: "Wondering where to begin when evaluating an AI tool for your software development lifecycle? Our cheatsheet of questions - and answers - has you covered."
tags: DevSecOps, AI/ML
guest: false
featured: yes
twitter_text: 'Wondering where to begin when evaluating an AI tool for your software development lifecycle? Our cheatsheet of questions - and answers - has you covered.'
postType:
related_posts:
---
AI enables organizations to enhance software development practices by boosting efficiency and reducing cycle times, but its use should not be at the cost of privacy and data security. Transparency around data protection and intellectual property should be a central part of any organization’s AI strategy. Transparency is even more critical for organizations using AI as part of DevOps as they need to know what they are agreeing to when using AI features and how updates will be communicated.

At GitLab, transparency is one of our [core values](https://handbook.gitlab.com/handbook/values/#transparency). As we continue to expand [GitLab Duo](https://about.gitlab.com/gitlab-duo/), our suite of AI-powered capabilities for the entire software development lifecycle, transparency remains a top priority.

GitLab’s [State of AI in Software Development report](https://about.gitlab.com/developer-survey/#ai) found that teams are feeling optimistic about their adoption of AI, and 83% of respondents said it is essential to implement AI in their software development processes to avoid falling behind. However, nearly as many respondents (79%) also expressed concern about AI tools having access to their private information and intellectual property. 

Many of our customers ask where they should begin when evaluating a new AI tool in their software development lifecycle. To help you get better visibility into the actions your DevOps provider is taking to protect your organization’s data and intellectual property, here are seven questions you can ask (as well as how GitLab Duo stacks up).

## 1. What large language models (LLMs) power the AI features in your platform?

Different LLMs have different strengths, so setting up your AI architecture with multiple models for specific use cases can be a path to success. However, it’s important to ensure that DevOps providers are transparent about the LLMs they utilize for their AI features as well as details about where the LLMs are hosted.

GitLab Duo features aren’t powered by a single model. We have built GitLab Duo with the flexibility to use the model that provides the best result for each use case. We continue our commitment to transparency by clearly identifying the models powering GitLab Duo features in our [publicly available documentation](https://docs.gitlab.com/ee/user/ai_features.html). 

## 2. Who has control of and access to the models? 

Every organization must be able to identify who has control of and access to the LLMs they are using. If a third party has control and access, are they listed by the DevOps provider as a subprocessor? If affiliates have control and access, are those affiliates clearly identified as a subprocessor? 

GitLab Duo is powered by third-party models hosted on cloud infrastructure, and the vendors of these models and the terms on which they provide services to GitLab were chosen as they support GitLab’s commitment to privacy and the protection of customer intellectual property.

We list all our subprocessors clearly on our [subprocessors page](https://about.gitlab.com/privacy/subprocessors/), and customers can [sign up](https://about.gitlab.com/privacy/subprocessors/#sign-up) to be notified when updates are made to this page.

## 3. What protection do you provide to alleviate customer concerns related to the perceived risks of using AI-generated output?

It's essential to know what protections a DevOps provider will provide regarding AI-generated output and how that guarantee will be met.

GitLab will indemnify you and protect your right to use output generated by GitLab Duo including defending you from claims that output generated from GitLab Duo infringes a third party’s intellectual property rights.

## 4. How do I get the benefit of those protections? Are the protections automatic, or do I need to take any action to receive the protections?

Even if you know that your DevOps provider includes protections related to the risks of using AI-generated output, it’s important to know what limitations, if any, are associated with those protections.

[GitLab protects your right to use output generated by GitLab Duo](https://about.gitlab.com/handbook/legal/ai-functionality-terms/) as long as you:
1. have not modified the output;
2. have a valid right to use your inputs;
3. have paid for the AI feature(s); and
4. have evaluated the output before using or otherwise relying on it.

At this time, you do not need to enable or activate any features or filters to receive this protection.

## 5. Do I retain my intellectual property (IP) rights to input entered into AI features?

IP is the foundation of an organization and, therefore, you must know how a DevOps provider will handle your rights in respect to inputs that you add to AI features. 

With GitLab Duo, your inputs remain your content. GitLab makes no claim of ownership in your input.

## 6. Do I own the output (or suggestions) generated from AI features?

Perhaps equally important is the question of whether you own what is generated from AI features — the output and suggestions — especially if they are incorporated into your software.

While the legal and regulatory landscape related to AI-generated output is developing, GitLab’s position is clear. GitLab does not claim ownership of any output generated by GitLab Duo. Output generated by GitLab Duo can be used at your discretion and, if a third-party claim arises from your use of the output generated GitLab Duo, GitLab will step in and defend you.

## 7. Where are the terms, policies, and commitments that govern the use of your AI features located?

DevOps providers should be able to share specific documentation about how their AI features use your data.

Here are the relevant resources for GitLab customers:
- [GitLab Subscription Agreement](https://about.gitlab.com/handbook/legal/subscription-agreement/)
- [AI Functionality Terms](https://about.gitlab.com/handbook/legal/ai-functionality-terms/)
- [GitLab Privacy Statement](https://about.gitlab.com/privacy/)
- [Acceptable Use Policy](https://about.gitlab.com/handbook/legal/acceptable-use-policy/)
- [GitLab Duo Documentation](https://docs.gitlab.com/ee/user/ai_features.html)

## Learn more
Without transparency from AI tool providers, organizations are unable to discern the risks around the handling of sensitive information and customer data, trade secrets, and the organization’s intellectual property rights. GitLab remains committed to privacy and transparency. With [GitLab Duo](https://about.gitlab.com/gitlab-duo/), enterprises and regulated organizations can adopt AI-powered workflows with confidence over how their sensitive data is being used.

You can learn more about GitLab’s privacy-first approach to AI in the [GitLab Duo documentation](https://docs.gitlab.com/ee/user/ai_features.html).
