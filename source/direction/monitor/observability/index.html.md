---
layout: markdown_page
title: "Product Direction - Analytics:Observability"
description: "The Product Direction for GitLab's Observability Group."
canonical_path: "/direction/analytics/observability/"
---

- TOC
{:toc}

Content Last Reviewed: 2023-11-06

## Overview

This page outlines the direction for the Monitor:Observability group. Feedback is welcome: you can [create a new issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/new) or reach out to this page maintainer if you have any questions or suggestions.

## Current Categories

| Category | Description | Availability | Maturity |
|----------|-------------|--------------|----------|
| [Error Tracking](/direction/analytics/observability/error_tracking/) | Detect, visualize and triage applications errors | GA | Minimal |
| Distributed Tracing | Track and visualize application requests across distributed services | Experiment | Minimal |
| Metrics | Collect and visualize application performance and health metrics | Experiment ETA Q4 2023 | Planned |
| Logging | Centralize application and infrastructure logs collection, storage and analysis | Experiment ETA Q1 2024 | Planned |

## What is Observability?

Observability software enables the visualization and analysis of application performance, availability and user experience, based on the telemetry data it generates, such as traces, metrics or logs. It allows developers and operators to understand the **impact of a code or configuration change**, answering questions such as:

- "What caused this failure?"
- "What caused this change in behavior?"
- "Why do a particular user’s requests fail?"
- "What are the performance bottlenecks impacting user experience?
- "Is my latest canary deployment stable?"

Common capabilities include the configuration of dashboards and alerts for issue detection and triage, the execution of analytical queries for deeper issue investigation and root cause analysis, as well as the capability to share findings with team members. By centralizing data from multiple sources into a single, unified platform, observability tools facilitate collaboration across software development and operations teams, enabling companies to resolve issues faster and minimize downtimes.

## Vision

We aim to provide a best-in-class observability solution that enables our customers to monitor their applications and infrastructure performance directly within GitLab. This will allow organizations to further consolidate their DevOps workflows into a single platform, enhancing operational efficiency.

## Principles

We will pursue this vision with the following principles:

#### Developer-first

We are focused on solving problems for [modern development teams](https://about.gitlab.com/handbook/product/personas/#sasha-software-developer) first. In doing so, we immediately add value to the majority of our current users. Over time, this will allows us to provide a unique, modern approach to observability, helping companies to further "shift-left" their monitoring workflows.

#### Support Platform engineers

[Platform teams](https://about.gitlab.com/handbook/product/personas/#priyanka-platform-engineer) are key to building an onramp to the product. Providing them capabilities to observe and improve the performance of their software delivery platform will allow us to land our first champion users within an organization, and help expanding to development teams.

#### Cloud-Native first

Building features first for [modern](https://about.gitlab.com/handbook/product/product-principles/#modern-first), [cloud-native](https://about.gitlab.com/handbook/product/product-principles/#cloud-native-first) environment allows us to focus on where development is going, and deliver solutions that every company aspires to use eventually.

#### Working by Default

We want developers to have immediate access to an available observability solution. That means we will ensure it is on by default, starts with convention over configuration, and is easy to use.

#### Unified Data Platform

Consolidating data into a single place facilitates collaboration and enables to surface unique insights by correlating diverse set of information, which would otherwise be overlooked if siloed. To achieve this, we are working to store all types of telemetry data that are useful for DevOps teams - from metrics, logs, traces, and errors, to [analytics data](https://about.gitlab.com/direction/analytics/cross-stage-vision.html) into a cohesive data platform that can span the entire DevOps process.

#### Integrated UI

The data visualization and investigation capabilities we are building are aimed to be seamlessly integrated into the platform and enhance other GitLab features and contribute to the direction for [customizable dashboards](/direction/customizable-dashboards/) in GitLab.

#### OpenTelemetry as the instrumentation standard

We are supporting [OpenTelemetry](https://opentelemetry.io/) as the primary way to instrument, generate, collect and export telemetry data. This approach aligns with GitLab's values, as it emphasizes our support for open-source industry standards. This way, we hope to contribute to improve OpenTelemetry, and that our customers will be able to do the same. We will progressively deprecate previously built solutions based on other agents or SDK.

## What has been released recently?

**Error Tracking is now Generally Available for SaaS (16.0 - 2023-05-22)**

GitLab error tracking allows developers to discover and view errors generated by their application directly within GitLab, removing the need to switch between multiple tools when debugging. In this release, we are supporting both the [GitLab integrated error tracking](https://docs.gitlab.com/ee/operations/error_tracking.html#integrated-error-tracking) and the [Sentry-based](https://docs.gitlab.com/ee/operations/error_tracking.html#sentry-error-tracking) backends.

Learn more: [Release Post](https://about.gitlab.com/releases/2023/05/22/gitlab-16-0-released/#error-tracking-is-now-generally-available), [Direction page](https://about.gitlab.com/direction/analytics/observability/error_tracking/)

**Distributed Tracing is now available as Experiment (16.2 - 2023-07-22)**

With GitLab Distributed Tracing, users can now troubleshoot application performance issues by inspecting how a request moves through different services and systems, the timing of each operation, and any errors as they occur. It is particularly useful in the context of micro-service applications, which group multiple independent services collaborating to fulfill user requests.

Since the Experiment release, we shipped the following new features over the last milestones - (16.3 > 16.5 - 2023-10-22):

Telemetry data can be collected via any OpenTelemetry SDK

* OpenTelemetry [code instrumentation is ]()[supported](https://opentelemetry.io/docs/instrumentation/) for many popular programming languages, making it very easy for users to send data to GitLab from any application.
* Collected Traces are instantly visible in GitLab, making debugging more efficient for users, and directly integrated within their development workflows.

Collected Traces can be queried and visualized via a new integrated UI, replacing previous Jaeger-based front-end

* Users can use advanced search queries to filter traces by attributes such as services, operation name, and time range.
* Service and operation attributes values are autocompleted as users type their search to simplify queries ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2379 "Add prepopulated Period filter to tracing search bar")).
* Results are displayed on a scatter plot, surfacing outliers such as slow traces and errors ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2384 "Show scatter chart on top of trace results")).
* Results are progressively loaded via continuous scrolling to facilitate navigation through large volume of data ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2272 "frontend: Add Infinite Scrolling to tracing list")).
* Individual traces are displayed as a waterfall diagram providing a complete view of a request distributed across different services and operations ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2253 "frontend: Trace Details UI")).

**Learn more:** [Documentation](https://docs.gitlab.com/ee/operations/tracing)

## What's coming next?

In the next 3 months (2023-11 - 2024-01), we plan to achieve GA for Distributed Tracing, and release the first versions of Metrics and Logs, unlocking new capabilities:

* Developers will be able to collect application Metrics and Logs in addition to Traces, offering a complete view of their application to facilitate the investigation of performance issues.
* Infrastructure operators will be able to start visualizing their infrastructure health in GitLab, by collecting data such as CPU utilization or memory usage and their related system logs, for troubleshooting infrastructure issue.
* Additionally, by collecting telemetry data from GitLab CI/CD pipelines, GitLab platform operators will be able to investigate performance issues in their Software Delivery platform, and optimize pipeline execution speed. ([related POC](https://gitlab.com/gitlab-org/gitlab/-/issues/386737#note_1262721406)).

**Distributed Tracing GA release**
* [Beta](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/78):
  * Finish UI requirements for Beta ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2364 "Tracing UI beta requirements"))
  * Finish migrating data store to ClickHouse Cloud ([epic](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/82 "Clickhouse cloud migration"))
  * Improve documentation to make new users onboarding easier ([issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/2439 "Update Tracing documentation with beta features"))
* [GA](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/79):
  * Usage tracking
  * Production readiness review
  * Licenses and entitlements

**Metrics Beta release**
* [Experiment](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/83):
  * Project-level metrics configuration
  * Collect metrics via OpenTelemetry SDK
  * List the collected metrics for a specific project
* [Beta](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/102):
  * Query Metrics real-time, historical and aggregated data
  * Visualize data as time-series chart

**Logs Experiment release** 
* [Experiment](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/96):
    * Project-level metrics configuration
    * Collect metrics via OpenTelemetry SDK
    * List recent logs for a specific project.

**Exploratory work**

During this period, we will also explore:
* What are the Jobs To Be Done for Observability ([issue](https://gitlab.com/gitlab-org/ux-research/-/issues/2661)), to help prioritizing future use cases and features we want to focus on.
* How users expect to discover and access Observability features within GitLab ([issue](https://gitlab.com/gitlab-org/ux-research/-/issues/2692)).
* Navigation solutions for GA features ([issue](https://gitlab.com/gitlab-org/ux-research/-/issues/2691)).

## What's our 1 year plan?

Our goal for the next year (2023-11 - 2024-11) is to achieve minimal maturity and GA for all Observability categories, so customers can further consolidate their monitoring workflows within GitLab.

While we don't expect customers to fully replace existing solutions they may use with higher level of maturity, we expect to be able to provide development teams that are currently not using any monitoring tools a basic set of capabilities available directly in GitLab. This way, they will be able to start tracking and improving their application performance and availability.

**Prioritized development work:**
* Collect and store main telemetry data types at enterprise scale: Traces, Logs, Metrics
* Run analytical queries for interactive exploration and analysis of these data
* Support self-managed customers via cloud connector integration

**Exploratory work:**
* Create and share [custom dashboards](https://about.gitlab.com/direction/customizable-dashboards/)
* Discover and map the instrumented application and infrastructure resources (service, hosts, environments...)

Learn more: [see epic](https://gitlab.com/groups/gitlab-org/opstrace/-/epics/92 "Observability Group - FY25 HQ")

## FAQ

1. **What is Opstrace?** On Dec 14, 2021, GitLab [acquired Opstrace](https://about.gitlab.com/press/releases/2021-12-14-gitlab-acquires-opstrace-to-expand-its-devops-platform-with-open-source-observability-solution.html) to accelerate GitLab's Observability roadmap. Opstrace product has been integrated into GitLab as the foundational component of our data management platform, on top of which we are now building upon additional capabilities.
2. **What is going to happen with the Opstrace name?** We are going to archive the name Opstrace as a standalone product, and instead focus on Observability features on the GitLab DevOps platform. Timeline and work that needs to be done are tracked in [this issue](https://gitlab.com/gitlab-org/opstrace/opstrace/-/issues/1761 "Archive the name Opstrace and focus on Observability in GitLab").
